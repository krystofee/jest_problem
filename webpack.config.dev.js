const webpack = require('webpack');
const path = require('path');

const REACT_DIR = path.resolve(__dirname, 'react');
const BUILD_DIR = path.resolve(__dirname, 'build');

const config = {

  entry: {
    bundle: path.join(REACT_DIR, 'index.jsx'),
  },

  output: {
    path: BUILD_DIR,
    filename: '[name].js',
  },

  module: {
    rules: [
      {
        test: /\.jsx?/,
        include: REACT_DIR,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [path.resolve(__dirname, 'react'), 'node_modules'],
  },
};

module.exports = config;
