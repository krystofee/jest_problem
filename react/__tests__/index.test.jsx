import React from 'react';
import { mount } from 'enzyme';
import TestComponent from '../index';

test('Sample test', () => {
  const wrapper = mount(
    <TestComponent />,
  );
  const header = wrapper.find('only-one-element').first();
  expect(header.text().toBe('component text'));
});
