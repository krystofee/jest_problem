const isTest = String(process.env.NODE_ENV) === 'test'

module.exports = {
  presets: [
    '@babel/react',
    [
      '@babel/preset-env',
      {
        modules: false,
        targets: {
          browsers: 'last 2 chrome versions, last 2 edge versions, last 2 firefox versions, last 2 safari versions, ie >= 9, last 2 chromeandroid versions, last 2 ios versions',
        },
      },
    ],
  ],
  plugins: [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    '@babel/plugin-proposal-object-rest-spread',
  ],
}
