import React from 'react';


class TestComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="only-one-element">component text</div>
    );
  }
}

TestComponent.propTypes = {};

export default TestComponent;
