module.exports = {
  displayName: 'Frontend tests',
  roots: ['react'],
  modulePaths: [
    'react',
  ],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  moduleDirectories: [
    'node_modules',
    'react',
  ],
};
